require('dotenv').config();

const express = require('express');
const morgan = require('morgan');


const {
    createUser,
    loginController,
} = require('./controllers/users');

const {
    allNotes,
    createNote,
    updateNote,
    getNoteById,
    deleteNote,
} = require('./controllers/notes');

const cors = require('cors');

const {authUser} = require('./middlewares/auth');

const app = express();

const bodyParser = require("body-parser");

app.use(bodyParser.json());

app.use(express.json());
app.use(morgan('dev'));
app.use(cors());

app.post('/user', createUser );
app.post('/login', loginController);

app.post('/', authUser, createNote);
app.get('/notes', authUser, allNotes);
app.get('/notes/:id', authUser, getNoteById);
app.put('/notes/:id', authUser, updateNote);
app.delete('/notes/:id', authUser, deleteNote);

app.use((req, res) => {
    res.status(404).send ({
        status: 'error',
        message: 'Not found', 
    });
});

app.use((error, req, res, next) => {
    console.error(error);

    res.status(error.httpStatus || 500).send({
        status: 'error',
        message: error.message
    });
});

app.listen(3001, () => {
    console.log('Servidor funcionando...');
});
