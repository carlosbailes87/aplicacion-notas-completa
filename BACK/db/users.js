const bcrypt = require('bcrypt');
const { generateError } = require('../helpers');
const {getConnection } = require('./db');

const  createNewUser = async (email, password) => {
    let connection;
    try {
        connection = await getConnection();
        const [user] = await connection.query(
            `
            SELECT id FROM users WHERE email = ?
            `,
            [email]
        );

        if (user.length > 0) {
            throw generateError('El email no está disponible.', 409);
        }

        const passwordHash = await bcrypt.hash(password, 4);

        const [newUser] = await connection.query(
            `
            INSERT INTO users (email, password) VALUES(?, ?)
            `,
            [email, passwordHash]
        );

        return newUser.insertId;

    } finally {
        if (connection) connection.release();
    }
};

const getUserByEmail = async (email) => {
    let connection;
    try {
        connection = await getConnection();

        const [login] = await connection.query(
            `
            SELECT * FROM users WHERE email = ?
         `,
            [email]
        );
        if (login.length === 0) {
            throw generateError('No hay usuarios registrados con este email.', 404);
        }
    
        return login[0];    
    } finally {
        if (connection) connection.release();
    }
  
};


module.exports = {
    createNewUser,
    getUserByEmail,
}