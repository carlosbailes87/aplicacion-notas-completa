const { getConnection } = require('./db');

const getAllNotes = async (userId) => {
    let connection;

    console.log('User id es', userId);

    try {
        connection = await getConnection();
        const [result] = await connection.query(`
            SELECT * FROM notes WHERE user_id = ? ORDER BY created_at DESC
        `,
        [userId]);
        return result;
        
    } finally {
        if (connection) connection.release();
    }
};

const createNoteDb = async (userId, text) => {
    let connection;

    try {
        connection = await getConnection();

        const [result] = await connection.query(`
            INSERT INTO notes (user_id, text)
            VALUES (?,?) 
        `,
            [userId, text]
        );

        return result.insertId;
    } finally {
        if (connection) connection.release();
    }
}; 

const deleteNoteDb = async (id) => {
    let connection;
  
    try {
      connection = await getConnection();
  
      await connection.query(
        `
        DELETE FROM notes WHERE id = ?
      `,
        [id]
      );
  
      return;
    } finally {
      if (connection) connection.release();
    }
};

const updateNoteDb = async (id, text) => {
let connection;

try {
    connection = await getConnection();
    
    await connection.query(`
    
    UPDATE notes SET text = ? WHERE id = ?

    `,

    [text, id]

    )     
        
}  finally {
    if (connection) connection.release();
    }
};

const getNoteDb = async (id) => {
    let connection;

    try {
        connection = await getConnection();

        const [result] = await connection.query(`

        SELECT * FROM notes WHERE id = ?

        `,
        [id]
        )
        return result[0];
    } finally {
        if (connection) connection.release();
    }
}; 

module.exports = {
    createNoteDb,
    getAllNotes,
    deleteNoteDb,
    getNoteDb,
    updateNoteDb,
};