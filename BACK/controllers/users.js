const { createNewUser, getUserByEmail } = require('../db/users');
const Joi = require('joi');
const { generateError } = require('../helpers')
const bcrypt = require ('bcrypt')
const jwt = require('jsonwebtoken');

const createUser = async (req, res, next) => {
    try {
        const {email, password} = req.body;
         
        const schema = Joi.object({
            email: Joi.string().email(),
            password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
        });
        const {schemaValidateError} = schema.validate(req.body);
        if (schemaValidateError) {
            throw schemaValidateError;
        }

        const id = await createNewUser(email, password);
        console.log(id);

        res.send({
            message: `Usuario creado con email y password ID: ${id}.`
        });
    } catch(error) {
        next(error);
    }
};

const loginController = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        const schema = Joi.object({
            email: Joi.string()
                .email(),
            password: Joi.string()
                .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
        })
        const {schemaValidateError} = schema.validate(req.body);
        if (schemaValidateError) {
            throw schemaValidateError;
        }

        const user = await getUserByEmail(email);
       
        const validPassword = await bcrypt.compare(password, user.password); 
        console.log(password, user.password);

        if (!validPassword) {
            throw generateError('La contraseña es erronea.', 401);
        }

        const payload = { id: user.id };

        const token = jwt.sign(payload, process.env.SECRET, {
            expiresIn: '30d',
        });

        res.send({
            status: 'ok',
            data: token
        });

    } catch(error) {
        next(error);
    }
};

module.exports = {
    createUser,
    loginController,
};