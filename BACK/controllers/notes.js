const { generateError } = require('../helpers');
const { createNoteDb, getAllNotes, deleteNoteDb, getNoteDb, updateNoteDb} = require('../db/notes');
const Joi = require('joi');

const allNotes = async (req, res, next) => {
    try {
        const notes = await getAllNotes(req.userId);
        res.send({
            status: 'ok',
            data: notes,
        });
    } catch(error) {
        next(error);
    }
};


const createNote = async (req, res, next) => {
      
    try {
        const { text } = req.body;

        const schema = Joi.object({
            text: Joi.string().min(4).max(350)
        });

        const { error } = schema.validate(req.body);
        if (error) {
            throw error;
        }

        const id = await createNoteDb(req.userId, text);
        res.send({
            status: 'ok',
            message: `Nota creada con id: ${id} correctamente`
        });
    } catch(error) {
        next(error);
    }
};


const updateNote = async (req, res, next) => {
    try {

        const { id } = req.params;
        const {text} = req.body;

        const schema = Joi.object({
            text: Joi.string().min(4).max(350)
        });
        const { error } = schema.validate(req.body);
        if (error) {
            throw error;
        }

        const note = await getNoteDb(id);
        
        
        if (req.userId !== note.user_id) {
            throw generateError(
                'Estás intentando actualizar una nota que no es tuya',
                401
            );
        }
        console.log('sting', req.body)
        await updateNoteDb(id, text);

        res.send({
            status: 'ok',
            message: 'Nota actualizada'
        });
    } catch(error) {
        next(error);
    }
};


const getNoteById = async (req, res, next) => {
    try {
        const { id } = req.params;

        const note = await getNoteDb(id);
        if (req.userId !== note.user_id) {
            throw generateError(
          'Estás intentando acceder a una nota que no es tuya',
          401
        );
      }
  
           res.send({
           status: 'ok',
           data: note,
      });
    } catch (error) {
      next(error);
    }
};


const deleteNote = async (req, res, next) => {
    
    try {
        
        const { id } = req.params;

        const note = await getNoteDb(id);
        
        if (req.userId !== note.user_id) {
            throw generateError(
          'Estás intentando borrar una nota que no es tuya',
          401
        );
      }
  
      await deleteNoteDb(id);
  
      res.send({
        status: 'ok',
        message: `La nota con id: ${id} fue borrado`,
      });
    } catch (error) {
      next(error);
    }
  };


module.exports = {
    allNotes,
    createNote,
    updateNote,
    getNoteById,
    deleteNote,
};