# APLICACIÓN NOTAS

## Para lanzar la aplicación introducir en terminal los comandos:

`npm install`

`npm run dev`

## Crear archivo `.env.local` en la carpeta `src` con el texto:

    VITE_API_BACK=http://localhost:3001

## En esta aplicación de notas, podrás guardar y gestionar tus notas. Tiene un formulario de creación de usuario, un formulario de login de usuario.
## Podrás gestionar tus notas, eliminando o actualizando el contenido de las mismas.

# RUTAS DE LA APLICACIÓN:

        
    "/" HomePage
    /register" RegisterPage
    /login" LoginPage
    /note/:id" NotePage
    /note/edit/:id" UpdateNotePage
    /user/:id" UserPage
    *" NotFoundPage


        