const mainURL = "http://localhost:3001";

export const getAllNotesService = async (token) => {
    const response = await fetch(`${mainURL}/notes`, {
      headers: {
        Authorization: token
      }
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  
  export const getSingleNoteService = async (id) => {
    const token = localStorage.getItem("token");
  
    const response = await fetch(`${mainURL}/notes/${id}`, {
      headers: {
        Authorization: token
      }
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  
  export const registerUserService = async ({ email, password }) => {
    const response = await fetch(`${mainURL}/user`, {
      method: "POST",
      body: JSON.stringify({ email, password }),
      headers: {
        "Content-Type": "application/json",
      },
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  };
  
  export const getUserDataService = async (id) => {
    const response = await fetch(`${mainURL}/users/${id}`);
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  
  export const logInUserService = async ({ email, password }) => {
    const response = await fetch(`${mainURL}/login`, {
      method: "POST",
      body: JSON.stringify({ email, password }),
      headers: {
        "Content-Type": "application/json",
      },
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  
  export const sendNoteService = async (text) => {
    const token = localStorage.getItem("token");
  
    const response = await fetch(`${mainURL}/`, {
      method: "POST",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ text })
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  
  export const deleteNoteService = async ({ id, token }) => {
    const response = await fetch(`${mainURL}/notes/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: token,
      },
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  };
  
  export const putUpdateNote = async ({ id, noteText}) => {
    const token = localStorage.getItem("token");
  
    const response = await fetch(`${mainURL}/notes/${id}`, {
      method: "PUT",
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        text: noteText
      })
    });
  
    const json = await response.json();
  
    if (!response.ok) {
      throw new Error(json.message);
    }
  
    return json.data;
  };
  