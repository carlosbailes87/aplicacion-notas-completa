import { useEffect, useState } from "react";
import { getAllNotesService } from "../services";

const useNotes = (id) => {
  const [notes, setNotes] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [token] = useState(localStorage.getItem("token"));

  useEffect(() => {
    loadNotes();
  }, [id]);

  const loadNotes = async () => {
    try {
      setLoading(true);

      if (!token) {
        throw new Error('Usuario no logeado');
      }

      const data = await getAllNotesService(token);
      setNotes(data);
    } catch (error) {
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  const addNote = (data) => {
    setNotes([data, ...notes]);
  };

  const removeNote = (id) => {
    setNotes(notes.filter((note) => note.id !== id));
  };

  const clearNotes = () => {
    setNotes([]);
  }

  return { notes, error, loading, loadNotes, addNote, removeNote, clearNotes };
};

export default useNotes;
