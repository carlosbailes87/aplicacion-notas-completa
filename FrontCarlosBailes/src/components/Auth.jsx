import { useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { useNavigate } from "react-router-dom";


export const Auth = () => {
  const { token, logout } = useContext(AuthContext);
  // const { clearNotes } = useNotes();
  const navigate = useNavigate();
  return token ? (
    <section>
      <button onClick={() => { logout(); navigate("/login")}}>Cerrar sesión</button>
    </section>
  ) : (
    <ul>
      <li>
        <Link to={"/register"}>Registrarse</Link>
      </li>
      <li>
        <Link to={"/login"}>Iniciar sesión</Link>
      </li>
    </ul>
  );
};
