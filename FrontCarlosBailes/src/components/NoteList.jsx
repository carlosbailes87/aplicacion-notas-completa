import { Note } from "./Note";

export const NoteList = ({ notes, removeNote }) => {
  return notes.length ? (
    <ul className="note-list">
      {notes.map((note) => {
        return (
          <li key={note.id}>
            <Note note={note} removeNote={removeNote} />
          </li>
        );
      })}
    </ul>
  ) : (
    <p>No hay notas...</p>
  );
};
