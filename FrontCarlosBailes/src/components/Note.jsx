import { Link, useNavigate } from "react-router-dom";
import { useContext, useState } from "react";
import { deleteNoteService } from "../services";
import { AuthContext } from "../context/AuthContext";

export const Note = ({ note, removeNote }) => {
  const navigate = useNavigate();
  const { token } = useContext(AuthContext);
  const [error, setError] = useState("");
  
  const deleteNote = async (id) => {
    try {
      await deleteNoteService({ id, token });

      if (removeNote) {
        removeNote(id);
      } else {
        navigate("/");
      }
    } catch (error) {
      setError(error.message);
    }
  };

  return (
    <article className="note">
      <p className="note-text">{note.text}</p>
      <p>
        Creada <Link to={`/user/${note.user_id}`}>{note.email}</Link> la fecha:{" "}
        <Link to={`/note/${note.id}`}>
          {new Date(note.created_at).toLocaleString()}
        </Link>
      </p>
      <section>
        <button
          onClick={() => {
            if (window.confirm("¿Estás seguro?")) deleteNote(note.id);
          }}
        >
          Borrar nota
        </button>
        
        <button onClick={() => {
          navigate(`note/edit/${note.id}`)
        }}>
          Actualizar Nota          
        </button>

        {error ? <p>{error}</p> : null}

      </section>
    </article>
  );
};
