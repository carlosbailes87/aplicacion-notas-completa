import { Auth } from "./Auth";
import { Link } from "react-router-dom";
import logoNota from "../assets/logoNota.png"

export const Header = () => {
  return (
    <header>
      <div className="app-title">
        <img src={logoNota} alt="Logo de nota" height={40}/>
        <h1>
          <Link to={"/"}>APP NOTAS</Link>
        </h1>
      </div>
      <nav>
        <Auth />
      </nav>
    </header>
  );
};