import { useState } from "react";
import { sendNoteService } from "../services";

export const NewNote = ({ addNote, loadNotes }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [text, setText] = useState("");

  const handleForm = async (e) => {
    e.preventDefault();

    try {
      setLoading(true);
      const note = await sendNoteService(text);

      loadNotes();
      setText("");

    } catch (error) {
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };
  return (
    <>
      <h1>Añadir nueva nota.</h1>
      <form className="new-note box" onSubmit={handleForm}>
        <fieldset>
          <label htmlFor="text">Tu nota aquí</label>
          <input
            type="text"
            name="text"
            id="text"
            required
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </fieldset>      
        <button>Guardar nota</button>
        {error ? <p>{error}</p> : null}
        {loading ? <p>Guardando nota...</p> : null}
      </form>
    </>
  );
};
