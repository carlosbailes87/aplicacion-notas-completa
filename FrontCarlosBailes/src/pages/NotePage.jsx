import { useParams } from "react-router-dom";
import useNote from "../hooks/useNote";
import { Note } from "../components/Note";
import { ErrorMessage } from "../components/ErrorMessage";
import { Loading } from "../components/Loading";

export const NotePage = () => {
  const { id } = useParams();
  const { note, error, loading } = useNote(id);

  if (loading) return <Loading />;
  if (error) return <ErrorMessage message={error} />;

  return (
    <section>
      <h1>Nota</h1>
      <Note note={note} />
    </section>
  );
};
