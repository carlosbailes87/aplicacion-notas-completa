import useNotes from "../hooks/useNotes";
import { NoteList } from "../components/NoteList";
import { NewNote } from "../components/NewNote";
import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { Loading } from "../components/Loading";
import  imagen  from "../assets/imagenNote.png"
import { Link } from "react-router-dom";

export const HomePage = () => {
  const { notes, error, loading, addNote, removeNote, loadNotes } = useNotes();
  const { token } = useContext(AuthContext);

  if (loading) return <Loading />;
  if (error) return (
    <div>
      <img className="imagen-nota" src={imagen} alt="Imagen de nota" />
        <div className="botones-inicio">
          <Link to="/login">
            <button>Iniciar sesión</button>
          </Link>

          <Link to="/register">
            <button>Registrarse</button>
          </Link>
        </div>
    </div>
  )
  return (
    <section>
      {token ? <NewNote addNote={addNote} loadNotes={loadNotes} /> : null}
      <h1>Últimas notas.</h1>
      <NoteList notes={notes} removeNote={removeNote} />
     
    </section>
  );
};
