import { useParams } from "react-router-dom";
import useNote from "../hooks/useNote";
import { ErrorMessage } from "../components/ErrorMessage";
import { Loading } from "../components/Loading";
import { useEffect, useState } from "react";
import { getSingleNoteService, putUpdateNote } from "../services";

import { useNavigate } from "react-router-dom";

export const UpdateNotePage = () => {
  const { id } = useParams();
  const { note, error, loading } = useNote(id);
  const [ noteText, setNoteText ] = useState("");
  const navigate = useNavigate();
  const handleUpdateNote = async(e) => {
    e.preventDefault();
    try {
      await putUpdateNote({ id, noteText });
      navigate("/")
    } catch(error) {
      console.log(error)
    }
  };
  
  useEffect(() => {
    const fetchData = async () => {
      const note = await getSingleNoteService(id);    
      setNoteText(note.text);
    };
    fetchData();
  }, [id]);

  if (loading) return <Loading />;
  if (error) return <ErrorMessage message={error} />;


  return (
    <section>
      <h1>Editar nota {id}</h1>
      <form onSubmit={handleUpdateNote}>
        
        <fieldset>
          <label>Edita tu nota.</label>
          <input 
            type="text"
            name="text"
            value={noteText}
            onChange={(e) => setNoteText(e.target.value)}>
          </input>          
        </fieldset>
        <button type="submit">Actualizar nota.          
        </button>
      </form>
      
    </section>
  );
};