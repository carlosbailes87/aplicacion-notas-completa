import { useParams } from "react-router-dom";
import useUser from "../hooks/useUser";
import { ErrorMessage } from "../components/ErrorMessage";
import { UserNotes } from "../components/UserNotes";
import { Loading } from "../components/Loading";

export const UserPage = () => {
  const { id } = useParams();
  const { user, loading, error } = useUser(id);

  if (loading) return <Loading />;
  if (error) return <ErrorMessage message={error} />;

  return (
    <section>
      <h1>Usuario {user.email}</h1>
      <section className="user-data">
        <p>Usuario id: {user.id}</p>
        <p>Registrado: {new Date(user.created_at).toLocaleString()}</p>
      </section>
      <UserNotes id={user.id} />
    </section>
  );
};
